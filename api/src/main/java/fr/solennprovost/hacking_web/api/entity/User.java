package fr.solennprovost.hacking_web.api.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstname;
    private String lastname;
    private String email;
    
    @Column(length = 50)
    private String password;

    private String role;
    private boolean isEnable;
}
