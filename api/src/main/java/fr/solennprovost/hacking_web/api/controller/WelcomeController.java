package fr.solennprovost.hacking_web.api.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping(path = "/welcome")
public class WelcomeController {

    @GetMapping
    public String welcome(){
        return "Welcome, you're connected";
    }
}
