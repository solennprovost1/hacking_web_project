import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import { CookieService } from 'ngx-cookie-service';
@Injectable({
    providedIn: 'root'
})
export class LoginService {

    public clientId = 'newClient';
    public redirectUri = 'http://localhost:8089/';

    constructor(private http: HttpClient,
                private cookie: CookieService) {
    }

    retrieveToken(code: string) {
        let params = new URLSearchParams();
        params.append('grant_type', 'authorization_code');
        params.append('client_id', this.clientId);
        params.append('client_secret', 'newClientSecret');
        params.append('redirect_uri', this.redirectUri);
        params.append('code', code);

        let headers =
            new HttpHeaders({'Content-type': 'application/x-www-form-urlencoded; charset=utf-8'});

        this.http.post('http://localhost:8081/login/oauth2/code/facebook',
            params.toString(), {headers: headers})
            .subscribe(
                data => this.saveToken(data),
                err => alert('Invalid Credentials'));
    }

    saveToken(token: any) {
        var expireDate = new Date().getTime() + (1000 * token.expires_in);
        this.cookie.set("access_token", token.access_token, expireDate);
        console.log('Obtained Access token');
        window.location.href = 'http://localhost:8089';
    }

    getResource(resourceUrl: string): Observable<any> {
        var headers = new HttpHeaders({
            'Content-type': 'application/x-www-form-urlencoded; charset=utf-8',
            'Authorization': 'Bearer ' + this.cookie.get('access_token')
        });
        return this.http.get(resourceUrl, {headers: headers})
    }

    checkCredentials() {
        return this.cookie.check('access_token');
    }

    logout() {
        this.cookie.delete('access_token');
        window.location.reload();
    }
}
