import { Component, OnInit } from '@angular/core';
import {LoginService} from "../../service/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public isLoggedIn = false;

  constructor(private service: LoginService) { }

  ngOnInit(): void {
    this.isLoggedIn = this.service.checkCredentials();
    let i = window.location.href.indexOf('code');
    if(!this.isLoggedIn && i != -1) {
      this.service.retrieveToken(window.location.href.substring(i + 5));
    }
  }

  login() {
    window.location.href =
        'http://localhost:8081/login/oauth2/code/facebook?response_type=code&scope=openid%20write%20read&client_id=' + this.service.clientId + '&redirect_uri='+ this.service.redirectUri;
  }

  logout() {
    this.service.logout();
  }


}
